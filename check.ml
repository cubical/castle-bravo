open Prelude
open Error
open Trace
open Ident
open Elab
open Expr

let ieq u v : bool = !Prefs.girard || u = v

let isProofIrrel = function
  | VApp (VApp (VId _, _), _) -> true
  | VBoundary _               -> true
  | _                         -> false

let vfst : value -> value = function
  | VPair (u, _)          -> u
  (* (meet p x H).1 ~> x *)
  | VMeet (_, x, _)       -> x
  | VMkEquiv (_, _, f, _) -> f
  | v                     -> VFst v

let vsnd : value -> value = function
  | VPair (_, u)          -> u
  | VMkEquiv (_, _, _, e) -> e
  | v                     -> VSnd v

(* Evaluator *)
let rec eval (e0 : exp) (ctx : ctx) = traceEval e0; match e0 with
  | EPre u                       -> VPre u
  | EKan u                       -> VKan u
  | EVar x                       -> getRho ctx x
  | EHole                        -> VHole
  | EPi  (a, (p, b))             -> let t = eval a ctx in VPi (t, (fresh p, closByVal ctx p t b))
  | ESig (a, (p, b))             -> let t = eval a ctx in VSig (t, (fresh p, closByVal ctx p t b))
  | ELam (a, (p, b))             -> let t = eval a ctx in VLam (t, (fresh p, closByVal ctx p t b))
  | EApp (f, x)                  -> app (eval f ctx, eval x ctx)
  | EPair (e1, e2)               -> VPair (eval e1 ctx, eval e2 ctx)
  | EFst e                       -> vfst (eval e ctx)
  | ESnd e                       -> vsnd (eval e ctx)
  | EId e                        -> VId (eval e ctx)
  | ERefl e                      -> VRefl (eval e ctx)
  | EJ e                         -> VJ (eval e ctx)
  | EPath (e, a, b)              -> VPath (eval e ctx, eval a ctx, eval b ctx)
  | EIdp e                       -> VIdp (eval e ctx)
  | ERev p                       -> rev (eval p ctx)
  | ETrans (p, q)                -> trans (eval p ctx, eval q ctx)
  | EBoundary (a, b, x)          -> VBoundary (eval a ctx, eval b ctx, eval x ctx)
  | ELeft (a, b)                 -> VLeft (eval a ctx, eval b ctx)
  | ERight (a, b)                -> VRight (eval a ctx, eval b ctx)
  | ESymm e                      -> symm (eval e ctx)
  | EComp (a, b)                 -> bcomp (eval a ctx) (eval b ctx)
  | EBLeft (e, p)                -> bleft (eval e ctx) (eval p ctx)
  | EBRight (e, p)               -> bright (eval e ctx) (eval p ctx)
  | EBApd (f, p, x, e)           -> bapd (eval f ctx) (eval p ctx) (eval x ctx) (eval e ctx)
  | EMeet (p, x, e)              -> meet (eval p ctx) (eval x ctx) (eval e ctx)
  | ECoe (p, x)                  -> coe (eval p ctx) (eval x ctx)
  | EApd (f, p)                  -> apd (eval f ctx) (eval p ctx)
  | EUAWeak (a, b, f, g, mu, nu) -> uaweak (eval a ctx) (eval b ctx) (eval f ctx) (eval g ctx) (eval mu ctx) (eval nu ctx)
  | Equiv (a, b)                 -> VEquiv (eval a ctx, eval b ctx)
  | EMkEquiv (a, b, f, e)        -> VMkEquiv (eval a ctx, eval b ctx, eval f ctx, eval e ctx)
  | EN                           -> VN
  | EZero                        -> VZero
  | ESucc                        -> VSucc
  | ENInd e                      -> VNInd (eval e ctx)
  | EZ                           -> VZ
  | EPos                         -> VPos
  | ENeg                         -> VNeg
  | EZSucc                       -> VZSucc
  | EZPred                       -> VZPred
  | EZInd e                      -> VZInd (eval e ctx)
  | ES1                          -> VS1
  | EBase                        -> VBase
  | ELoop                        -> VLoop
  | ES1Ind e                     -> VS1Ind (eval e ctx)
  | ES1IndS e                    -> VS1IndS (eval e ctx)
  | ER                           -> VR
  | Elem                         -> VElem
  | EGlue                        -> VGlue
  | ERInd e                      -> VRInd (eval e ctx)
  | ERIndS e                     -> VRIndS (eval e ctx)
  | ERInj                        -> VRInj
  | EBot                         -> VBot
  | EBotRec e                    -> VBotRec (eval e ctx)

and bcomp a b  = reduceBoundary (VComp (a, b))
and bleft v p  = reduceBoundary (VBLeft (v, p))
and bright v p = reduceBoundary (VBRight (v, p))

and trans = function
  | VTrans (p, q), r       -> trans (p, trans (q, r))
  | VIdp _, p | p, VIdp _  -> p
  | VRev p, VTrans (q, r)  -> if conv p q then r else VTrans (VRev p, VTrans (q, r))
  | p, VTrans (VRev q, r)  -> if conv p q then r else VTrans (p, VTrans (VRev q, r))
  | VRev p, q              -> if conv p q then let (_, _, v) = extPath (inferV p) in VIdp v else VTrans (VRev p, q)
  | p, VRev q              -> if conv p q then let (_, v, _) = extPath (inferV p) in VIdp v else VTrans (p, VRev q)
  | VUAWeak (a, b, f1, g1, mu1, nu1), VUAWeak (_, c, f2, g2, mu2, nu2) ->

    let f = VLam (a, (freshName "x", fun x -> app (f2, app (f1, x)))) in
    let g = VLam (c, (freshName "x", fun x -> app (g1, app (g2, x)))) in

    let mu = VLam (a, (freshName "x", fun x ->
      trans (ap b (fun y -> app (g1, y))
        (app (g2, app (f2, app (f1, x)))) (app (f1, x))
        (app (mu2, app (f1, x))), app (mu1, x)))) in

    let nu = VLam (c, (freshName "x", fun x ->
      trans (ap b (fun y -> app (f2, y))
          (app (f1, (app (g1, (app (g2, x)))))) (app (g2, x))
          (app (nu1, app (g2, x))), app (nu2, x)))) in

    VUAWeak (a, c, f, g, mu, nu)
  | p, q -> VTrans (p, q)

and rev : value -> value = function
  | VUAWeak (a, b, f, g, mu, nu) -> VUAWeak (b, a, g, f, nu, mu)
  | VRev p        -> p
  | VIdp v        -> VIdp v
  | VTrans (p, q) -> trans (rev q, rev p)
  | v             -> VRev v

and symm = function
  (* ∂-symm (left a b) ~> right b a *)
  | VLeft (a, b) -> VRight (b, a)
  (* ∂-symm (right a b) ~> left b a *)
  | VRight (a, b) -> VLeft (b, a)
  (* ∂-symm (∂-symm H) ~> H *)
  | VSymm v -> v
  | v -> VSymm v

and bapdleft f p t a b =
  coe (apd (apdlam t a b (freshName "x") (freshName "σ") (fun x y ->
    inferV (app2 f x y))) p) (app2 f a (VLeft (a, b)))

and bapdright f a b = app2 f b (VRight (a, b))

and bapdcenter f p t b x y =
  coe (apd (apdlam t x b (freshName "x") (freshName "σ") (fun x' y' ->
    inferV (app2 f x' (bcomp y y'))))
      (rev (vsnd (meet (rev p) x (symm y))))) (app2 f x y)

and bapd f p x = function
  (* ∂-apd f p a (left a b) ~> left (coe (apd β p) (f a (left a b))) (f b (right a b)) *)
  | VLeft (a, b) -> VLeft (bapdleft f p (inferV a) a b, bapdright f a b)
  (* ∂-apd f p b (right a b) ~> right (coe (apd β p) (f a (left a b))) (f b (right a b)) *)
  | VRight (a, b) -> VRight (bapdleft f p (inferV a) a b, bapdright f a b)
  | y -> VBApd (f, p, x, y)

and reduceBoundary v =
  let (a, b, x) = extBoundary (inferV v) in
  if conv a x then VLeft (a, b)
  else if conv b x then VRight (a, b) else v

and ap t f a b p = let x = freshName "x" in
  congr t a b x Irrefutable (f (Var (x, t))) p

and coe p x = match p, x with
  (* coe (idp α) x ~> x *)
  | VIdp _, _ -> x
  (* coe (p ⬝ q) x ~> coe q (coe p x) *)
  | VTrans (q, p), _ -> coe p (coe q x)
  (* coe (ua-weak a b f g α β) x ~> f x *)
  | VUAWeak (_, _, f, _, _, _), _ -> app (f, x)
  | VRev (VApd (VLam (t, (x, f)), q)), v -> let g = f (Var (x, t)) in
    let (k, _) = extPi (inferV g) in let (a, b, _) = extBoundary k in let y = freshName "σ" in
    let y' = Var (y, VBoundary (b, a, Var (x, t))) in let h = app (g, symm y') in
    begin match h with
      | VPath _ | VPi _ | VSig _ | VEquiv _ ->
        transport (rev q) t b a x y h v
      | _ -> VCoe (p, v)
    end
  | VApd (VLam (t, (x, f)), q), v -> let g = f (Var (x, t)) in
    let (k, _) = extPi (inferV g) in let (a, b, _) = extBoundary k in
    let y = freshName "σ" in let y' = Var (y, k) in
    transport q t a b x y (app (g, y')) v
  | _, _ -> VCoe (p, x)

and transport p t a b x y g v = match p, g with
  | VIdp _, _ -> v
  | _, VPath (k, f, g) ->
    let k' x' y' = subst (rho2 x x' y y') k in
    let f' x' y' = subst (rho2 x x' y y') f in
    let g' x' y' = subst (rho2 x x' y y') g in

    let p1 = congr t a b x y f p in
    let p3 = congr t a b x y g p in

    let p2 = ap (k' a (VLeft (a, b))) (transport p t a b x y k)
      (f' a (VLeft (a, b))) (g' a (VLeft (a, b))) v in

    trans (rev p1, trans (p2, p3))

  | _, VPi (k, (_, f)) ->
    let k' x' y' = subst (rho2 x x' y y') k in
    let f' x' y' z = subst (rho2 x x' y y') (f z) in

    let y1 = freshName "y" in let y2 = freshName "y′" in let y3 = freshName "y″" in
    let h1 = freshName "σ" in let h2 = freshName "σ′" in let h3 = freshName "σ″" in

    let y1' = Var (y1, t) in let h1' = Var (h1, VBoundary (a, b, y1')) in
    let y2' = Var (y2, t) in let h2' = Var (h2, VBoundary (b, y1', y2')) in
    let y3' = Var (y3, t) in let h3' = Var (h3, VBoundary (b, a, y3')) in

    let phi x =
      transport p t a b y1 h1
        (f' y1' h1' (transport (vsnd (meet (rev p) y1' (symm h1')))
          t b y1' y2 h2 (k' y2' (bcomp h1' (symm h2'))) x))
        (app (v, transport (rev p) t b a y3 h3 (k' y3' (symm h3')) x)) in

    VLam (k' b (VRight (a, b)), (freshName "x", phi))

  | _, VSig (k, (_, f)) ->
    let k' x' y' = subst (rho2 x x' y y') k in
    let f' x' y' z = subst (rho2 x x' y y') (f z) in

    let y1 = freshName "y" in let y2 = freshName "y′" in
    let h1 = freshName "σ" in let h2 = freshName "σ′" in

    let y1' = Var (y1, t) in let h1' = Var (h1, VBoundary (a, b, y1')) in
    let y2' = Var (y2, t) in let h2' = Var (h2, VBoundary (a, y1', y2')) in

    let fst = transport p t a b x y k (vfst v) in

    let snd = transport p t a b y1 h1
      (f' y1' h1' (transport (vsnd (meet p y1' h1')) t a y1' y2 h2
        (k' y2' (symm (bcomp (symm h1') (symm h2')))) (vfst v))) (vsnd v) in

    VPair (fst, snd)

  | _, VEquiv (t1, t2) ->
    let (fst, snd) = extPair (transport p t a b x y
      (VSig (implv t1 t2, (freshName "e", biinv t1 t2))) v) in

    let rho = rho2 x b y (VRight (a, b)) in
    VMkEquiv (subst rho t1, subst rho t2, fst, snd)

  | _, _ -> let q = congr t a b x y g p in
    if isCoeNeut q then VCoe (q, v) else coe q v

and closByVal ctx p t e v = traceClos e p v;
  (* dirty hack to handle free variables introduced by type checker *)
  let ctx' = match v with
  | Var (x, t) -> if Env.mem x ctx then ctx else upLocal ctx x t v
  | _          -> ctx in
  eval e (upLocal ctx' p t v)

and meet p x v =
  (* “∂ A a b x” is proof-irrelevant,
     so each element of “∂ a b a” is definitionally equal to “left a b”,
     each element of “∂ a b b” — to “right a b” *)
  let (a, b, y) = extBoundary (inferV v) in
  (* meet p a left  ~> (a, idp a) *)
  if conv a y then VPair (x, VIdp x)
  (* meet p b right ~> (b, p) *)
  else if conv b y then VPair (x, p)
  else VMeet (p, x, v)

and extCongLam t =
  let (dom, (x, f)) = extPi t in
  let (n, (y, g)) = extPi (f (Var (x, dom))) in
  let cod = g (Var (y, n)) in
  (dom, cod, x, y, extBoundary n)

and congr t a b x y g p =
  (* apd id p ~> p *)
  if convVar x g then p
  (* apd (λ _, x) p ~> idp x *)
  else if not (mem x g || mem y g) then VIdp g
  else match g, p with
  (* apd f (idp x) ~> idp (f x) *)
  | _, VIdp z -> VIdp (subst (rho2 x z y (VLeft (z, z))) g)

  (* apd f p⁻¹ ~> (apd f p)⁻¹ *)
  | _, VRev p -> let k x' y' = inferV (subst (rho2 x x' y y') g) in
    let x1 = freshName "y"  in let y1 = freshName "σ"  in
    let x2 = freshName "y′" in let y2 = freshName "σ′" in

    let x1' = Var (x1, t) in let y1' = Var (y1, VBoundary (b, a, x1')) in
    let x2' = Var (x2, t) in let y2' = Var (y2, VBoundary (x1', b, x2')) in

    rev (congr t b a x1 y1 (transport (rev (vsnd (meet p x1' y1'))) t x1' b
      x2 y2 (k x2' (bcomp (symm y1') y2')) (subst (rho2 x x1' y (symm y1')) g)) p)

  (* apd f (p ⬝ q) ~> apd f p ⬝ apd f q *)
  | _, VTrans (p, q) ->
    let (_, a, b) = extPath (inferV p) in
    let (_, _, c) = extPath (inferV q) in

    let g' x' y' = subst (rho2 x x' y y') g in
    let k x' y' = inferV (g' x' y') in

    let x1 = freshName "y"  in let y1 = freshName "σ"  in
    let x2 = freshName "y′" in let y2 = freshName "σ′" in
    let x3 = freshName "y″" in let y3 = freshName "σ″" in
    let x4 = freshName "y‴" in let y4 = freshName "σ‴" in

    let x1' = Var (x1, t) in let y1' = Var (y1, VBoundary (a, b, x1')) in
    let x2' = Var (x2, t) in let y2' = Var (y2, VBoundary (a, b, x2')) in
    let x3' = Var (x3, t) in let y3' = Var (y3, VBoundary (b, c, x3')) in
    let x4' = Var (x4, t) in let y4' = Var (y4, VBoundary (b, c, x4')) in

    let p1 = congr t a b x1 y1 (g' x1' (bright y1' q)) p in
    let p2 = congr (k b (bright (VRight (a, b)) q))
      (transport p t a b x2 y2 (k x2' (bright y2' q))
        (g' a (bright (VLeft (a, b)) q))) (g' b (bright (VRight (a, b)) q)) x1 y1
      (transport q t b c x3 y3 (k x3' (bleft y3' (rev p))) x1') p1 in
    let p3 = congr t b c x4 y4 (g' x4' (bleft y4' (rev p))) q in

    trans (p2, p3)

  (* apd f (apd g p) ~> apd (f ∘ g) p *)
  | _, VApd (h, p) ->
    let (t, a, b) = extPath (inferV p) in
    let x1 = freshName "y" in let y1 = freshName "σ" in

    let x' = Var (x, t) in let y' = Var (y, VBoundary (a, b, x')) in
    let x1' = Var (x1, t) in let y1' = Var (y1, VBoundary (x', b, x1')) in

    congr t a b x y
      (subst (rho2 x (transport (rev (vsnd (meet (rev p) x' (symm y')))) t x' b
        x1 y1 (inferV (app2 h x1' (bcomp y' y1'))) (app2 h x' y'))
      y (bapd h p x' y')) g) p

  | VApp (VApp (VApp (VS1Ind k, b), l), z), VLoop ->
    if convVar x z && not (mem2 x y k || mem2 x y b || mem2 x y l) then l
    else VApd (apdlam t a b x y (fun x' y' -> subst (rho2 x x' y y') g), p)

  | VApp (VApp (VApp (VRInd k, cz), sz), z), VApp (VGlue, z') ->
    if convVar x z && not (mem2 x y k || mem2 x y cz || mem2 x y sz) then
      app (sz, z')
    else VApd (apdlam t a b x y (fun x' y' -> subst (rho2 x x' y y') g), p)

  | _, _ -> VApd (apdlam t a b x y (fun x' y' -> subst (rho2 x x' y y') g), p)

and apd f p = let (t, a, b) = extPath (inferV p) in
  let x = freshName "x" in let x' = Var (x, t) in
  let g = app (f, x') in let (k, _) = extPi (inferV g) in
  let y = freshName "σ" in let h = app (g, Var (y, k)) in
  congr t a b x y h p

and apdlam t a b x y g = VLam (t, (x, fun x -> VLam (VBoundary (a, b, x), (y, g x))))

and uaweak a b f g mu nu =
  match f, g, mu, nu with
  | VLam (_, (x, f')), VLam (_, (y, g')),
    VLam (_, (n, mu')), VLam (_, (m, nu')) ->
    let v = Var (n, a) in let w = Var (m, b) in

    (* ua-weak (idfun A) (idfun A) idp idp ~> idp A *)
    if convVar x (f' (Var (x, a))) &&
       convVar y (g' (Var (y, b))) &&
       conv (VIdp v) (mu' v) && conv (VIdp w) (nu' w)
    then VIdp a else VUAWeak (a, b, f, g, mu, nu)
  | _ -> VUAWeak (a, b, f, g, mu, nu)

and app (f, x) = match f, x with
  (* (λ (x : t), f) v ~> f[x/v] *)
  | VLam (_, (_, f)), v -> f v
  (* N-ind A z s zero ~> z *)
  | VApp (VApp (VNInd _, z), _), VZero -> z

  (* N-ind A z s (succ n) ~> s (N-ind A z s n) *)
  | VApp (VApp (VNInd _, _), s), VApp (VSucc, n) -> app (app (s, n), app (f, n))
  (* Z-ind A p n (pos x) ~> p x *)
  | VApp (VApp (VZInd _, p), _), VApp (VPos, x) -> app (p, x)
  (* Z-ind A p n (neg x) ~> n x *)
  | VApp (VApp (VZInd _, _), n), VApp (VNeg, x) -> app (n, x)

  (* Z-succ (neg (succ n)) ~> neg n *)
  | VZSucc, VApp (VNeg, VApp (VSucc, n)) -> negv n
  (* Z-succ (neg zero) ~> pos zero *)
  | VZSucc, VApp (VNeg, VZero) -> posv VZero
  (* Z-succ (pos n) ~> pos (succ n) *)
  | VZSucc, VApp (VPos, n) -> posv (succv n)
  (* Z-pred (neg n) ~> neg (succ n) *)
  | VZPred, VApp (VNeg, n) -> negv (succv n)
  (* Z-pred (pos zero) ~> neg zero *)
  | VZPred, VApp (VPos, VZero) -> negv VZero
  (* Z-pred (pos (succ n)) ~> pos n *)
  | VZPred, VApp (VPos, VApp (VSucc, n)) -> posv n
  (* Z-succ (Z-pred z) ~> z *)
  | VZSucc, VApp (VZPred, z) -> z
  (* Z-pred (Z-succ z) ~> z *)
  | VZPred, VApp (VZSucc, z) -> z

  (* S¹-ind β b ℓ base ~> b *)
  | VApp (VApp (VS1Ind _, b), _), VBase -> b

  (* R-ind β cz sz (elem z) ~> cz z *)
  | VApp (VApp (VRInd _, cz), _), VApp (VElem, z) -> app (cz, z)
  (* R-inj x x (refl (elem x)) ~> refl x *)
  | VApp (VApp (VRInj, x), _), VRefl _ -> VRefl x

  | _, _ -> VApp (f, x)

and app2 f x y = app (app (f, x), y)

and getRho ctx x = match Env.find_opt x ctx with
  | Some (_, _, Value v) -> v
  | Some (_, _, Exp e)   -> eval e ctx
  | None                 -> raise (VariableNotFound x)

(* This is part of evaluator, not type checker *)
and inferV v = traceInferV v; match v with
  | Var (_, t) -> t
  | VLam (t, (x, f)) -> VPi (t, (x, fun x -> inferV (f x)))
  | VPi (t, (x, f)) | VSig (t, (x, f)) -> imax (inferV t) (inferV (f (Var (x, t))))
  | VFst e -> inferFst (inferV e)
  | VSnd e -> inferSnd (vfst e) (inferV e)
  | VCoe (p, _) -> let (_, _, t) = extPath (inferV p) in t
  | VMeet (p, _, _) -> let (t, a, _) = extPath (inferV p) in singl t a
  | VLeft (a, b) -> VBoundary (a, b, a)
  | VRight (a, b) -> VBoundary (a, b, b)
  | VApd (f, p) -> inferVApd f p (inferV p) (inferV f)
  | VSymm v -> let (a, b, x) = extBoundary (inferV v) in VBoundary (b, a, x)
  | VBLeft (v, p) -> let (_, b, x) = extBoundary (inferV v) in let (_, _, a) = extPath (inferV p) in VBoundary (a, b, x)
  | VBRight (v, p) -> let (a, _, x) = extBoundary (inferV v) in let (_, _, b) = extPath (inferV p) in VBoundary (a, b, x)
  | VBApd (f, p, x, y) -> inferVBApd f p (inferV p) x y
  | VComp (u, v) -> let (a, b, _) = extBoundary (inferV u) in
    let (_, _, y) = extBoundary (inferV v) in VBoundary (a, b, y)
  | VId t -> let n = extSet (inferV t) in implv t (implv t (VPre n))
  | VJ t -> inferJ (inferV t)
  | VApp (f, x) -> let (_, (_, g)) = extPi (inferV f) in g x
  | VRefl v -> idv (inferV v) v v
  | VIdp v -> VPath (inferV v, v, v)
  | VRev p -> let (v, a, b) = extPath (inferV p) in VPath (v, b, a)
  | VTrans (p, q) -> let (t, a, _) = extPath (inferV p) in let (_, _, c) = extPath (inferV q) in VPath (t, a, c)
  | VPre n -> VPre (Z.succ n)
  | VKan n -> VKan (Z.succ n)
  | VPath (v, _, _) -> inferV v
  | VBoundary (v, _, _) -> let n = extSet (inferV (inferV v)) in VPre n
  | VUAWeak (a, b, _, _, _, _) -> VPath (inferV a, a, b)
  | VEquiv (a, _) -> inferV (inferV a)
  | VMkEquiv (a, b, _, _) -> VEquiv (a, b)
  | VN -> VKan Z.zero | VZero -> VN | VSucc -> implv VN VN
  | VNInd v -> inferNInd v
  | VZ -> VKan Z.zero | VPos -> implv VN VZ | VNeg -> implv VN VZ
  | VZSucc -> implv VZ VZ | VZPred -> implv VZ VZ | VZInd v -> inferZInd v
  | VS1 -> VKan Z.zero | VBase -> VS1 | VLoop -> VPath (VS1, VBase, VBase)
  | VS1Ind v -> inferS1Ind v | VS1IndS v -> inferS1IndS v
  | VR -> VKan Z.zero | VElem -> implv VZ VR | VGlue -> inferGlue ()
  | VRInd v -> inferRInd v | VRIndS v -> inferRIndS v | VRInj -> inferRInj ()
  | VBot -> VKan Z.zero | VBotRec v -> implv VBot v
  | VPair _ | VHole -> raise (InferVError v)

and inferJ v = let n = extSet v in
  let x = freshName "x" in let y = freshName "y" in
  let pi = freshName "P" in let p = freshName "p" in

  let t = VPi (v, (x, fun x ->
    VPi (v, (y, fun y -> implv (idv v x y) (VPre n))))) in

  VPi (t, (pi, fun pi ->
    VPi (v, (x, fun x ->
      implv (app (app (app (pi, x), x), VRefl x))
            (VPi (v, (y, fun y ->
              VPi (idv v x y, (p, fun p ->
                app (app (app (pi, x), y), p))))))))))

and inferVApd f p t1 t2 =
  let (t, a, b) = extPath t1 in

  let (_, (_, g)) = extPi t2 in
  let (_, (_, h)) = extPi (g b) in
  let k = h (VRight (a, b)) in

  let fa = app (app (f, a), VLeft (a, b)) in
  let fb = app (app (f, b), VRight (a, b)) in
  VPath (k, coe (apd (VLam (t, (freshName "x", fun x ->
    VLam (VBoundary (a, b, x), (freshName "H", fun y ->
      let (_, (_, g)) = extPi (g x) in g y))))) p) fa, fb)

and inferVBApd f p k x y = let (t, a, b) = extPath k in
  VBoundary (bapdleft f p t a b, bapdright f a b, bapdcenter f p t b x y)

and inferNInd v =
  let e = fun x -> app (v, x) in
  implv (e VZero)
    (implv (VPi (VN, (freshName "n", fun n -> implv (e n) (e (succv n)))))
           (VPi (VN, (freshName "n", e))))

and inferZInd v =
  let e = fun x -> app (v, x) in
  implv (VPi (VN, (freshName "n", e << posv)))
    (implv (VPi (VN, (freshName "n", e << negv)))
      (VPi (VZ, (freshName "z", e))))

and inferS1Ind v =
  let e = fun x -> app (v, x) in
  VPi (e VBase, (freshName "b", fun b ->
    implv (VPath (e VBase, coe (ap VS1 e VBase VBase VLoop) b, b))
          (VPi (VS1, (freshName "x", e)))))

and inferS1IndS v = failwith "not implemented yet"

and zsuccv z = app (VZSucc, z)

and inferGlue () =
  let z = freshName "z" in
  VPi (VZ, (z, fun z -> VPath (VR, elemv z, elemv (zsuccv z))))

and inferRInd v =
  let e = fun x -> app (v, x) in
  let cz = freshName "cz" in
  VPi (VPi (VZ, (freshName "z", e << elemv)), (cz, fun cz ->
    implv (VPi (VZ, (freshName "z", fun z ->
      VPath (e (elemv (zsuccv z)),
        coe (apd (VLam (VR, (freshName "x", fun x ->
          VLam (VBoundary (elemv z, elemv (zsuccv z), x),
            (freshName "y", fun _ -> e x))))) (VApp (VGlue, z))) (app (cz, z)), app (cz, zsuccv z)))))
      (VPi (VR, (freshName "z", e)))))

and inferRIndS v = failwith "not implemented yet"

and inferRInj () =
  let x = freshName "x" in let y = freshName "y" in
  VPi (VZ, (x, fun x -> VPi (VZ, (y, fun y -> implv (idv VR (elemv x) (elemv y)) (idv VZ x y)))))

and inferFst = function
  | VSig (t, _)   -> t
  | VEquiv (a, b) -> implv a b
  | v             -> raise (ExpectedSig v)

and inferSnd v = function
  | VSig (_, (_, g)) -> g v
  | VEquiv (a, b)    -> biinv a b v
  | u                -> raise (ExpectedSig u)

(* Convertibility *)
and conv v1 v2 : bool = traceConv v1 v2;
  v1 == v2 || begin match v1, v2 with
    | VKan u, VKan v -> ieq u v
    | VPair (a, b), VPair (c, d) -> conv a c && conv b d
    | VPair (a, b), v | v, VPair (a, b) -> conv (vfst v) a && conv (vsnd v) b
    | VPi  (a, (p, f)), VPi  (b, (_, g))
    | VSig (a, (p, f)), VSig (b, (_, g))
    | VLam (a, (p, f)), VLam (b, (_, g)) ->
      let x = Var (p, a) in conv a b && conv (f x) (g x)
    | VLam (a, (p, f)), b | b, VLam (a, (p, f)) ->
      let x = Var (p, a) in conv (app (b, x)) (f x)
    | VApp (f, x), VApp (g, y) -> conv f g && conv x y
    | VPre u, VPre v -> ieq u v
    | Var (u, _), Var (v, _) -> u = v
    | VFst x, VFst y | VSnd x, VSnd y -> conv x y
    | VId u, VId v | VJ u, VJ v | VRefl u, VRefl v -> conv u v
    | VPath (t1, a1, b1), VPath (t2, a2, b2) -> conv t1 t2 && conv a1 a2 && conv b1 b2
    | VIdp a, VIdp b -> conv a b
    | VRev p, VRev q -> conv p q
    | VTrans (p1, q1), VTrans (p2, q2) -> conv p1 p2 && conv q1 q2
    | VBoundary (a1, b1, x1), VBoundary (a2, b2, x2) -> conv a1 a2 && conv b1 b2 && conv x1 x2
    | VSymm a, VSymm b -> conv a b
    | VLeft (a1, b1), VLeft (a2, b2) | VRight (a1, b1), VRight (a2, b2) -> conv a1 a2 && conv b1 b2
    | VBLeft (a1, b1), VBLeft (a2, b2) | VBRight (a1, b1), VBRight (a2, b2)
    | VComp (a1, b1), VComp (a2, b2) -> conv a1 a2 && conv b1 b2
    | VBApd (f1, p1, x1, v1), VBApd (f2, p2, x2, v2) -> conv f1 f2 && conv p1 p2 && conv x1 x2 && conv v1 v2
    | VMeet (p1, x1, v1), VMeet (p2, x2, v2) -> conv p1 p2 && conv x1 x2 && conv v1 v2
    | VCoe (p1, x1), VCoe (p2, x2) -> conv p1 p2 && conv x1 x2
    | VApd (f1, p1), VApd (f2, p2) -> conv f1 f2 && conv p1 p2
    | VUAWeak (a1, b1, f1, g1, mu1, nu1), VUAWeak (a2, b2, f2, g2, mu2, nu2) ->
      conv a1 a2 && conv b1 b2 && conv f1 f2 && conv g1 g2 && conv mu1 mu2 && conv nu1 nu2
    | VEquiv (a1, b1), VEquiv (a2, b2) -> conv a1 a2 && conv b1 b2
    | VMkEquiv (_, _, f1, v1), VMkEquiv (_, _, f2, v2) -> conv f1 f2 && conv v1 v2
    | VMkEquiv (_, _, f, v), u | u, VMkEquiv (_, _, f, v) -> conv (vfst u) f && conv (vsnd u) v
    | VN, VN -> true
    | VZero, VZero -> true
    | VSucc, VSucc -> true
    | VNInd u, VNInd v -> conv u v
    | VZ, VZ -> true
    | VPos, VPos -> true
    | VNeg, VNeg -> true
    | VZSucc, VZSucc -> true
    | VZPred, VZPred -> true
    | VZInd u, VZInd v -> conv u v
    | VS1, VS1 -> true
    | VBase, VBase -> true
    | VLoop, VLoop -> true
    | VS1Ind u, VS1Ind v -> conv u v
    | VS1IndS u, VS1IndS v -> conv u v
    | VR, VR -> true
    | VElem, VElem -> true
    | VGlue, VGlue -> true
    | VRInd u, VRInd v -> conv u v
    | VRIndS u, VRIndS v -> conv u v
    | VRInj, VRInj -> true
    | VBot, VBot -> true
    | VBotRec u, VBotRec v -> conv u v
    | _, _ -> false
  end || convProofIrrel v1 v2

and convProofIrrel v1 v2 =
  (* Id A a b is proof-irrelevant *)
  try let t1 = inferV v1 in let t2 = inferV v2 in
    if isProofIrrel t1 && isProofIrrel t2
    then conv t1 t2 else false
  with InferVError _ -> false

and eqNf v1 v2 : unit = traceEqNF v1 v2;
  if conv v1 v2 then () else raise (Ineq (v1, v2))

(* Type checker itself *)
and lookup (x : name) (ctx : ctx) = match Env.find_opt x ctx with
  | Some (_, Value v, _) -> v
  | Some (_, Exp e, _)   -> eval e ctx
  | None                 -> raise (VariableNotFound x)

and check ctx (e0 : exp) (t0 : value) =
  traceCheck e0 t0; try match e0, t0 with
  | ELam (a, (p, b)), VPi (t, (_, g)) ->
    ignore (extSet (infer ctx a)); eqNf (eval a ctx) t;
    let x = Var (p, t) in let ctx' = upLocal ctx p t x in check ctx' b (g x)
  | EPair (e1, e2), VSig (t, (_, g)) ->
    ignore (extSet (inferV t));
    check ctx e1 t; check ctx e2 (g (eval e1 ctx))
  | EHole, v -> traceHole v ctx
  | ERefl e, VApp (VApp (VId t, a), b) | EIdp e, VPath (t, a, b) ->
    check ctx e t; let v = eval e ctx in eqNf v a; eqNf v b
  | ERev p, VPath (t, a, b) -> check ctx p (VPath (t, b, a))
  | ETrans (p, q), VPath (t, a, c) ->
    let (u, x, y1) = extPath (infer ctx p) in let (v, y2, z) = extPath (infer ctx q) in
    eqNf u t; eqNf v t; eqNf y1 y2; eqNf x a; eqNf z c
  | e, VPre u -> begin
    match infer ctx e with
    | VKan v | VPre v -> if ieq u v then () else raise (Ineq (VPre u, VPre v))
    | t -> raise (Ineq (VPre u, t))
  end
  | ECoe (p, x), t2 ->
    let t1 = infer ctx x in let u1 = inferV t1 in let u2 = inferV t2 in
    eqNf u1 u2; ignore (extKan (inferV t1));
    check ctx p (VPath (u1, t1, t2))
  | e, t -> eqNf (infer ctx e) t
  with ex -> Printf.printf "When trying to typecheck\n  %s\nAgainst type\n  %s\n" (showExp e0) (showValue t0); raise ex

and infer ctx e : value = traceInfer e; try match e with
  | EVar x -> lookup x ctx
  | EKan u -> VKan (Z.succ u)
  | ESig (a, (p, b)) | EPi (a, (p, b)) -> inferTele ctx p a b
  | ELam (a, (p, b)) -> inferLam ctx p a b
  | EApp (f, x) -> begin match infer ctx f with
    | VPi (t, (_, g)) -> check ctx x t; g (eval x ctx)
    | v -> raise (ExpectedPi v)
  end
  | EFst e -> inferFst (infer ctx e)
  | ESnd e -> inferSnd (vfst (eval e ctx)) (infer ctx e)
  | EPre u -> VPre (Z.succ u)
  | EPath (e, a, b) -> let t = eval e ctx in check ctx a t; check ctx b t; infer ctx e
  | EId e -> let v = eval e ctx in let n = extSet (infer ctx e) in implv v (implv v (VPre n))
  | ERefl e -> let v = eval e ctx in idv (infer ctx e) v v
  | EJ e -> inferJ (infer ctx e)
  | EIdp e -> let v = eval e ctx in let t = infer ctx e in VPath (t, v, v)
  | ERev p -> let (v, a, b) = extPath (infer ctx p) in VPath (v, b, a)
  | ETrans (p, q) -> let (u, a, x) = extPath (infer ctx p) in let (v, y, c) = extPath (infer ctx q) in
    eqNf u v; eqNf x y; VPath (u, a, c)
  | EBoundary (a, b, x) -> let t = infer ctx a in check ctx b t; check ctx x t; let n = extSet (inferV t) in VPre n
  | ELeft (a, b) -> let t = infer ctx a in check ctx b t; let x = eval a ctx in let y = eval b ctx in VBoundary (x, y, x)
  | ERight (a, b) -> let t = infer ctx a in check ctx b t; let x = eval a ctx in let y = eval b ctx in VBoundary (x, y, y)
  | ESymm e -> inferSymm ctx e
  | EBLeft (e, p) -> let (a, b, x) = extBoundary (infer ctx e) in
    let (_, a', c) = extPath (infer ctx p) in eqNf a a'; VBoundary (c, b, x)
  | EBRight (e, p) -> let (a, b, x) = extBoundary (infer ctx e) in
    let (_, b', c) = extPath (infer ctx p) in eqNf b b'; VBoundary (a, c, x)
  | EBApd (f, p, x, y) -> let k = infer ctx p in let (t, a, b) = extPath k in
    let (t', (x', g)) = extPi (infer ctx f) in
    let (s', (y', h)) = extPi (g (Var (x', t'))) in

    eqNf t t'; eqNf (VBoundary (a, b, Var (x', t'))) s';
    ignore (extKan (inferV (h (Var (y', s')))));

    let x' = eval x ctx in check ctx x t; check ctx y (VBoundary (a, b, x'));
    inferVBApd (eval f ctx) (eval p ctx) k x' (eval y ctx)
  | EComp (a, b) -> inferComp ctx a b
  | EMeet (p, x, e) -> inferMeet ctx p x e
  | ECoe (p, x) -> let (e, a, b) = extPath (infer ctx p) in ignore (extKan e); check ctx x a; b
  | EApd (f, p) -> inferApd ctx f p
  | EUAWeak (a, b, f, g, mu, nu) -> inferUAWeak ctx a b f g mu nu
  | Equiv (a, b) -> let t1 = infer ctx a in let t2 = infer ctx b in ignore (extSet t1); eqNf t1 t2; inferV t1
  | EMkEquiv (a, b, f, e) -> inferMkEquiv ctx a b f e
  | EN -> VKan Z.zero | EZero -> VN | ESucc -> implv VN VN
  | ENInd e -> inferInd false ctx VN e inferNInd
  | EZ -> VKan Z.zero | EPos -> implv VN VZ | ENeg -> implv VN VZ
  | EZSucc -> implv VZ VZ | EZPred -> implv VZ VZ
  | EZInd e -> inferInd false ctx VZ e inferZInd
  | ES1 -> VKan Z.zero | EBase -> VS1 | ELoop -> VPath (VS1, VBase, VBase)
  | ES1Ind e -> inferInd true ctx VS1 e inferS1Ind
  | ES1IndS e -> inferInd false ctx VS1 e inferS1IndS
  | ER -> VKan Z.zero | Elem -> implv VZ VR | EGlue -> inferGlue ()
  | ERInd e -> inferInd true ctx VR e inferRInd
  | ERIndS e -> inferInd false ctx VR e inferRIndS
  | ERInj -> inferRInj ()
  | EBot -> VKan Z.zero | EBotRec e -> ignore (extSet (infer ctx e)); implv VBot (eval e ctx)
  | EPair _ | EHole -> raise (InferError e)
  with ex -> Printf.printf "When trying to infer type of\n  %s\n" (showExp e); raise ex

and inferInd fibrant ctx t e f =
  let (t', (p, g)) = extPi (infer ctx e) in eqNf t t'; let k = g (Var (p, t)) in
  ignore (if fibrant then extKan k else extSet k); f (eval e ctx)

and inferTele ctx p a b =
  ignore (extSet (infer ctx a));
  let t = eval a ctx in let x = Var (p, t) in
  let ctx' = upLocal ctx p t x in
  let v = infer ctx' b in imax (infer ctx a) v

and inferLam ctx p a e =
  ignore (extSet (infer ctx a)); let t = eval a ctx in
  ignore (infer (upLocal ctx p t (Var (p, t))) e);
  VPi (t, (p, fun x -> infer (upLocal ctx p t x) e))

and inferSymm ctx e =
  let (a, b, x) = extBoundary (infer ctx e) in VBoundary (b, a, x)

and inferComp ctx a b =
  let (a1, b1, x1) = extBoundary (infer ctx a) in
  let (a2, b2, x2) = extBoundary (infer ctx b) in
  eqNf b1 b2; eqNf x1 a2; VBoundary (a1, b1, x2)

and singl t x = let y = freshName "y" in VSig (t, (y, fun y -> VPath (t, x, y)))

and inferMeet ctx p x e =
  let (t, a, b) = extPath (infer ctx p) in
  let (a', b', x') = extBoundary (infer ctx e) in
  check ctx x t; eqNf a a'; eqNf b b'; eqNf (eval x ctx) x'; singl t a

and linv a b f =
  let g = freshName "g" in let x = freshName "x" in
  VSig (implv b a, (g, fun g ->
    VPi (a, (x, fun x -> VPath (a, app (g, app (f, x)), x)))))

and rinv a b f =
  let h = freshName "h" in let x = freshName "x" in
  VSig (implv b a, (h, fun h ->
    VPi (b, (x, fun x -> VPath (b, app (f, app (h, x)), x)))))

and biinv a b f = prodv (linv a b f) (rinv a b f)

and inferApd ctx f p =
  let t1 = infer ctx p in
  let t2 = infer ctx f in

  let (t, a, b) = extPath t1 in
  let (t', (x, g')) = extPi t2 in
  let (r, (y, h')) = extPi (g' (Var (x, t'))) in

  eqNf t t'; eqNf r (VBoundary (a, b, Var (x, t')));
  ignore (extKan (inferV (h' (Var (y, r)))));

  inferVApd (eval f ctx) (eval p ctx) t1 t2

and inferUAWeak ctx a b f g mu nu =
  let t = infer ctx a in let t' = infer ctx b in eqNf t t';
  let a' = eval a ctx in let b' = eval b ctx in
  check ctx f (implv a' b'); check ctx g (implv b' a');

  let f' = eval f ctx in let g' = eval g ctx in

  check ctx mu (VPi (a', (freshName "x", fun x ->
    VPath (a', app (g', app (f', x)), x))));
  check ctx nu (VPi (b', (freshName "x", fun x ->
    VPath (b', app (f', app (g', x)), x))));

  VPath (t, a', b')

and inferMkEquiv ctx a b f e =
  let t1 = infer ctx a in let t2 = infer ctx b in eqNf t1 t2;
  let (a', (x, b')) = extPi (infer ctx f) in let b'' = b' (Var (x, a')) in

  eqNf (eval a ctx) a'; eqNf (eval b ctx) b''; let f' = eval f ctx in
  check ctx e (biinv a' b'' f'); VEquiv (a', b'')

and mem x = function
  | Var (y, _) -> x = y
  | VSig (t, (p, f)) | VPi (t, (p, f)) | VLam (t, (p, f)) ->
    mem x t || mem x (f (Var (p, t)))
  | VBot   | VKan _ | VPre _ | VHole
  | VS1    | VBase  | VLoop
  | VR     | VElem  | VGlue | VRInj
  | VN     | VZero  | VSucc
  | VZ     | VPos   | VNeg
  | VZSucc | VZPred -> false

  | VFst e    | VSnd e    | VId e
  | VRefl e   | VJ e      | VIdp e
  | VRev e    | VSymm e   | VNInd e
  | VZInd e   | VS1Ind e  | VRInd e
  | VBotRec e | VS1IndS e | VRIndS e -> mem x e

  | VPair (a, b)  | VComp (a, b) | VApp (a, b)
  | VCoe (a, b)   | VApd (a, b)  | VTrans (a, b)
  | VEquiv (a, b) | VLeft (a, b) | VRight (a, b)
  | VBLeft (a, b) | VBRight (a, b) -> mem x a || mem x b

  | VPath (a, b, c) | VBoundary (a, b, c) | VMeet (a, b, c) -> mem x a || mem x b || mem x c

  | VBApd (a, b, c, d) | VMkEquiv (a, b, c, d) -> mem x a || mem x b || mem x c || mem x d

  | VUAWeak (a, b, f, g, mu, nu) -> mem x a || mem x b || mem x f || mem x g || mem x mu || mem x nu

and mem2 x y v = mem x v || mem y v

and subst rho = function
  | VPre n                       -> VPre n
  | VKan n                       -> VKan n
  | VHole                        -> VHole
  | VApp (f, x)                  -> app (subst rho f, subst rho x)
  | VPi (t, (p, f))              -> VPi (subst rho t, (p, fun x -> subst rho (f x)))
  | VSig (t, (p, f))             -> VSig (subst rho t, (p, fun x -> subst rho (f x)))
  | VLam (t, (p, f))             -> VLam (subst rho t, (p, fun x -> subst rho (f x)))
  | VPair (a, b)                 -> VPair (subst rho a, subst rho b)
  | VFst v                       -> vfst (subst rho v)
  | VSnd v                       -> vsnd (subst rho v)
  | VId v                        -> VId (subst rho v)
  | VRefl v                      -> VRefl (subst rho v)
  | VJ v                         -> VJ (subst rho v)
  | VPath (e, a, b)              -> VPath (subst rho e, subst rho a, subst rho b)
  | VIdp e                       -> VIdp (subst rho e)
  | VRev p                       -> rev (subst rho p)
  | VTrans (p, q)                -> trans (subst rho p, subst rho q)
  | VBoundary (a, b, x)          -> VBoundary (subst rho a, subst rho b, subst rho x)
  | VLeft (a, b)                 -> VLeft (subst rho a, subst rho b)
  | VRight (a, b)                -> VRight (subst rho a, subst rho b)
  | VSymm e                      -> symm (subst rho e)
  | VComp (a, b)                 -> bcomp (subst rho a) (subst rho b)
  | VBLeft (e, p)                -> bleft (subst rho e) (subst rho p)
  | VBRight (e, p)               -> bright (subst rho e) (subst rho p)
  | VBApd (f, p, x, e)           -> bapd (subst rho f) (subst rho p) (subst rho x) (subst rho e)
  | VMeet (p, x, e)              -> meet (subst rho p) (subst rho x) (subst rho e)
  | VCoe (p, x)                  -> coe (subst rho p) (subst rho x)
  | VApd (f, p)                  -> apd (subst rho f) (subst rho p)
  | VUAWeak (a, b, f, g, mu, nu) -> uaweak (subst rho a) (subst rho b) (subst rho f) (subst rho g) (subst rho mu) (subst rho nu)
  | VEquiv (a, b)                -> VEquiv (subst rho a, subst rho b)
  | VMkEquiv (a, b, f, e)        -> VMkEquiv (subst rho a, subst rho b, subst rho f, subst rho e)
  | VN                           -> VN
  | VZero                        -> VZero
  | VSucc                        -> VSucc
  | VNInd v                      -> VNInd (subst rho v)
  | VZ                           -> VZ
  | VPos                         -> VPos
  | VNeg                         -> VNeg
  | VZSucc                       -> VZSucc
  | VZPred                       -> VZPred
  | VZInd v                      -> VZInd (subst rho v)
  | VS1                          -> VS1
  | VBase                        -> VBase
  | VLoop                        -> VLoop
  | VS1Ind v                     -> VS1Ind (subst rho v)
  | VS1IndS v                    -> VS1IndS (subst rho v)
  | VR                           -> VR
  | VElem                        -> VElem
  | VGlue                        -> VGlue
  | VRInd v                      -> VRInd (subst rho v)
  | VRIndS v                     -> VRIndS (subst rho v)
  | VRInj                        -> VRInj
  | VBot                         -> VBot
  | VBotRec v                    -> VBotRec (subst rho v)
  | Var (x, t)                   -> begin match Env.find_opt x rho with
    | Some v -> v
    | None   -> Var (x, t)
  end
