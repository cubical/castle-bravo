\documentclass[a4paper,UKenglish,cleveref, autoref, thm-restate]{lipics-v2021}
\bibliographystyle{plainurl}

\usepackage{mathtools}
\usepackage{amsmath}

\newcommand{\UU}{\ensuremath{\mathcal{U}}}
\newcommand{\VV}{\ensuremath{\mathcal{V}}}

\newcommand{\ct}{%
  \mathchoice{\mathbin{\raisebox{0.5ex}{$\displaystyle\centerdot$}}}%
             {\mathbin{\raisebox{0.5ex}{$\centerdot$}}}%
             {\mathbin{\raisebox{0.25ex}{$\scriptstyle\,\centerdot\,$}}}%
             {\mathbin{\raisebox{0.1ex}{$\scriptscriptstyle\,\centerdot\,$}}}
}

\title{HoTT-\ensuremath{\mathrm{\partial}} Type System with definitional Path-\ensuremath{\mathrm{\beta}}}
\titlerunning{Castle Bravo}
\author{Namdak Tonpa}{Groupoid Infinity}{maxim@synrc.com}{https://orcid.org/0000-0001-7127-8796}{}
\author{Siegmentation Fault}{Groupoid Infinity}{siegmentationfault@yandex.ru}{}{}

\authorrunning{J.\,Q. Public and J.\,R. Public} %TODO mandatory. First: Use abbreviated first/middle names. Second (only in severe cases): Use first author plus 'et al.'

\Copyright{John Q. Public and Joan R. Public} %TODO mandatory, please use full first names. LIPIcs license is "CC-BY";  http://creativecommons.org/licenses/by/3.0/

\ccsdesc[100]{\textcolor{red}{Replace ccsdesc macro with valid one}} %TODO mandatory: Please choose ACM 2012 classifications from https://dl.acm.org/ccs/ccs_flat.cfm 

\keywords{Homotopy Type System, Cubical Type Theory}

\category{} %optional, e.g. invited paper

\relatedversion{} %optional, e.g. full version hosted on arXiv, HAL, or other respository/website
%\relatedversiondetails[linktext={opt. text shown instead of the URL}, cite=DBLP:books/mk/GrayR93]{Classification (e.g. Full Version, Extended Version, Previous Version}{URL to related version} %linktext and cite are optional

%\supplement{}%optional, e.g. related research data, source code, ... hosted on a repository like zenodo, figshare, GitHub, ...
%\supplementdetails[linktext={opt. text shown instead of the URL}, cite=DBLP:books/mk/GrayR93, subcategory={Description, Subcategory}, swhid={Software Heritage Identifier}]{General Classification (e.g. Software, Dataset, Model, ...)}{URL to related version} %linktext, cite, and subcategory are optional

%\funding{(Optional) general funding statement \dots}%optional, to capture a funding statement, which applies to all authors. Please enter author specific funding statements as fifth argument of the \author macro.

\acknowledgements{I want to thank \dots}%optional

\nolinenumbers

%\hideLIPIcs  %uncomment to remove references to LIPIcs series (logo, DOI, ...), e.g. when preparing a pre-final version to be uploaded to arXiv or another public repository

%Editor-only macros:: begin (do not touch as author)%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\EventEditors{John Q. Open and Joan R. Access}
\EventNoEds{2}
\EventLongTitle{42nd Conference on Very Important Topics (CVIT 2016)}
\EventShortTitle{CVIT 2016}
\EventAcronym{CVIT}
\EventYear{2016}
\EventDate{December 24--27, 2016}
\EventLocation{Little Whinging, United Kingdom}
\EventLogo{}
\SeriesVolume{42}
\ArticleNo{23}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\maketitle

\begin{abstract}
The HTS\footnote{\url{https://www.math.ias.edu/vladimir/sites/math.ias.edu.vladimir/files/HTS.pdf} -- HTS}
language proposed by Voevodsky exposes two different presheaf models of type theory:
the inner one is homotopy type system presheaf that models HoTT and the outer one is traditional Martin-Löf
type system presheaf that models set theory with UIP. The motivation behind this doubling is to
have an ability to express semisemplicial types. Theoretical work on merging inner
and outer languages was continued in 2LTT\footnote{\url{https://arxiv.org/pdf/1705.03307.pdf} -- 2LTT}.
\end{abstract}

\section{Introduction}
\label{sec:typesetting-summary}

\begin{equation*}
    \frac{}{\Gamma \vdash \UU_i : \UU_{i + 1}}
    \quad
    \frac{}{\Gamma \vdash \VV_i : \VV_{i + 1}}
    \quad
    \frac{\Gamma \vdash A : \UU_i}{\Gamma \vdash A : \VV_i}
\end{equation*}

\begin{equation*}
    \frac{\Gamma \vdash A : \UU \hspace{10pt} \Gamma \vdash a : A \hspace{10pt} \Gamma \vdash b : A}
         {\Gamma \vdash a = b : \UU}
    \quad
    \frac{\Gamma \vdash A : \UU \hspace{10pt} \Gamma \vdash a : A}
         {\Gamma \vdash \mathrm{idp}_A(a) : a = a}
\end{equation*}

\begin{equation*}
    \frac{\begin{matrix}
            \Gamma \vdash A : \UU \hspace{10pt} \Gamma \vdash a : A \hspace{10pt} \Gamma \vdash b : A \\
            \Gamma \vdash p : a = b
          \end{matrix}}
         {\Gamma \vdash p^{-1} : b = a}
    \quad
    \frac{\begin{matrix}
            \Gamma \vdash A : \UU \hspace{10pt} \Gamma \vdash a : A \hspace{10pt} \Gamma \vdash b : A \hspace{10pt} \Gamma \vdash c : A \\
            \Gamma \vdash p : a = b \hspace{10pt} \Gamma \vdash q : b = c
          \end{matrix}}
         {\Gamma \vdash p \ct q : a = c}
\end{equation*}

\begin{equation*}
    \frac{\begin{matrix}
            \Gamma \vdash A : \UU \hspace{10pt} \Gamma \vdash a : A \hspace{10pt} \Gamma \vdash b : A \hspace{10pt} \Gamma \vdash c : A \hspace{10pt} \Gamma \vdash d : A \\
            \Gamma \vdash p : a = b \hspace{10pt} \Gamma \vdash q : b = c \hspace{10pt} \Gamma \vdash r : c = d
          \end{matrix}}
         {\Gamma \vdash (p \ct q) \ct r \equiv p \ct (q \ct r) : a = d}
\end{equation*}

\begin{equation*}
    \frac{\Gamma \vdash A : \UU \hspace{10pt} \Gamma \vdash a : A \hspace{10pt} \Gamma \vdash b : A \hspace{10pt} \Gamma \vdash c : A \hspace{10pt} \Gamma \vdash p : a = b \hspace{10pt} \Gamma \vdash q : b = c}
         {\Gamma \vdash (p \ct q)^{-1} \equiv q^{-1} \ct p^{-1} : c = a}
\end{equation*}

\begin{equation*}
    \frac{\Gamma \vdash A : \UU \hspace{10pt} \Gamma \vdash a : A \hspace{10pt} \Gamma \vdash b : A \hspace{10pt} \Gamma \vdash c : A \hspace{10pt} \Gamma \vdash p : a = b \hspace{10pt} \Gamma \vdash q : b = c}
         {\Gamma \vdash p \ct (p^{-1} \ct q) \equiv q : b = c}
\end{equation*}

\begin{equation*}
    \frac{\Gamma \vdash A : \UU \hspace{10pt} \Gamma \vdash a : A \hspace{10pt} \Gamma \vdash b : A \hspace{10pt} \Gamma \vdash c : A \hspace{10pt} \Gamma \vdash p : a = b \hspace{10pt} \Gamma \vdash q : b = c}
         {\Gamma \vdash p^{-1} \ct (p \ct q) \equiv q : b = c}
\end{equation*}

\begin{equation*}
    \frac{\Gamma \vdash A : \UU \hspace{10pt} \Gamma \vdash a : A \hspace{10pt} \Gamma \vdash b : A \hspace{10pt} \Gamma \vdash p : a = b}
         {\Gamma \vdash p \ct p^{-1} \equiv \mathrm{idp}_A(a) : a = a}
\end{equation*}

\begin{equation*}
    \frac{\begin{matrix}
            \Gamma \vdash A : \UU \hspace{10pt} \Gamma \vdash a : A \hspace{10pt} \Gamma \vdash b : A \\
            \Gamma \vdash p : a = b
          \end{matrix}}
         {\Gamma \vdash p^{-1} \ct p \equiv \mathrm{idp}_A(b) : b = b}
    \quad
    \frac{\begin{matrix}
            \Gamma \vdash A : \UU \hspace{10pt} \Gamma \vdash a : A \hspace{10pt} \Gamma \vdash b : A \\
            \Gamma \vdash p : a = b
          \end{matrix}}
         {\Gamma \vdash (p^{-1})^{-1} \equiv p : a = b}
\end{equation*}

\begin{equation*}
    \frac{\Gamma \vdash A : \UU \hspace{10pt} \Gamma \vdash x : A}
         {\Gamma \vdash (\mathrm{idp}_A(x))^{-1} \equiv \mathrm{idp}_A(x) : x = x}
\end{equation*}

\begin{equation*}
    \frac{\begin{matrix}
            \Gamma \vdash A : \UU \hspace{10pt} \Gamma \vdash a : A \hspace{10pt} \Gamma \vdash b : A \\
            \Gamma \vdash p : a = b
          \end{matrix}}
         {\Gamma \vdash \mathrm{idp}_A(a) \ct p \equiv p : a = b}
    \quad
    \frac{\begin{matrix}
            \Gamma \vdash A : \UU \hspace{10pt} \Gamma \vdash a : A \hspace{10pt} \Gamma \vdash b : A \\
            \Gamma \vdash p : a = b
          \end{matrix}}
         {\Gamma \vdash p \ct \mathrm{idp}_A(b) \equiv p : a = b}
\end{equation*}

\begin{equation*}
    \frac{\Gamma \vdash A : \UU \hspace{10pt} \Gamma \vdash a : A \hspace{10pt} \Gamma \vdash b : A \hspace{10pt} \Gamma \vdash x : A}
         {\Gamma \vdash \partial_A(a, b, x) : \VV}
\end{equation*}

\begin{equation*}
    \frac{\begin{matrix}
            \Gamma \vdash A : \UU \hspace{10pt} \Gamma \vdash a : A \hspace{10pt} \Gamma \vdash b : A \hspace{10pt} \Gamma \vdash x : A \\
            \Gamma \vdash p : \partial_A(a, b, x) \hspace{10pt} \Gamma \vdash q : \partial_A(a, b, x)
          \end{matrix}}
         {\Gamma \vdash p \equiv q}
\end{equation*}

\begin{equation*}
    \frac{\Gamma \vdash A : \UU \hspace{10pt} \Gamma \vdash a : A \hspace{10pt} \Gamma \vdash b : A}
         {\Gamma \vdash \mathrm{left}_A(a, b) : \partial_A(a, b, a)}
    \quad
    \frac{\Gamma \vdash A : \UU \hspace{10pt} \Gamma \vdash a : A \hspace{10pt} \Gamma \vdash b : A}
         {\Gamma \vdash \mathrm{right}_A(a, b) : \partial_A(a, b, b)}
\end{equation*}

\begin{equation*}
    \frac{\Gamma \vdash A : \UU \hspace{10pt} \Gamma \vdash a : A \hspace{10pt} \Gamma \vdash b : A \hspace{10pt} \Gamma \vdash x : A}
         {\Gamma \vdash \partial_{\mathrm{symm}} : \partial_A(a, b, x) \rightarrow \partial_A(b, a, x)}
\end{equation*}

\begin{equation*}
    \frac{\Gamma \vdash A : \UU \hspace{10pt} \Gamma \vdash a : A \hspace{10pt} \Gamma \vdash b : A \hspace{10pt} \Gamma \vdash x : A \hspace{10pt} \Gamma \vdash y : A}
         {\Gamma \vdash \partial_{\mathrm{comp}} : \partial_A(a, b, x) \rightarrow \partial_A(x, b, y) \rightarrow \partial_A(a, b, y)}
\end{equation*}

\begin{equation*}
    \frac{\Gamma \vdash A : \UU \hspace{10pt} \Gamma \vdash a : A \hspace{10pt} \Gamma \vdash b : A \hspace{10pt} \Gamma \vdash c : A \hspace{10pt} \Gamma \vdash x : A}
         {\Gamma \vdash \partial_{\mathrm{left}} : \partial_A(a, b, x) \rightarrow a = c \rightarrow \partial_A(c, b, x)}
\end{equation*}

\begin{equation*}
    \frac{\Gamma \vdash A : \UU \hspace{10pt} \Gamma \vdash \Gamma a : A \hspace{10pt} \Gamma \vdash b : A \hspace{10pt} \Gamma \vdash c : A \hspace{10pt} \Gamma \vdash x : A}
         {\Gamma \vdash \partial_{\mathrm{right}} : \partial_A(a, b, x) \rightarrow b = c \rightarrow \partial_A(a, c, x)}
\end{equation*}

\begin{equation*}
    \frac{\Gamma \vdash A : \UU \hspace{10pt} \Gamma \vdash B : \UU}
         {\Gamma \vdash \mathrm{coe} : A = B \rightarrow A \rightarrow B}
\end{equation*}

\begin{equation*}
    \mathrm{singl}(A, a) \coloneqq \sum_{b : A} a = b
\end{equation*}

\begin{equation*}
    \frac{\Gamma \vdash A : \UU \hspace{10pt} \Gamma \vdash a : A \hspace{10pt} \Gamma \vdash b : A \hspace{10pt} \Gamma \vdash p : a = b \hspace{10pt} \Gamma \vdash x : A \hspace{10pt} \Gamma \vdash H : \partial_A(a, b, x)}
         {\Gamma \vdash \mathrm{meet}(p, x, H) : \mathrm{singl}(A, a)}
\end{equation*}

\begin{equation*}
    \frac{\Gamma \vdash A : \UU \hspace{10pt} \Gamma \vdash a : A \hspace{10pt} \Gamma \vdash b : A \hspace{10pt} \Gamma \vdash p : a = b \hspace{10pt} \Gamma \vdash a \not\equiv b}
         {\Gamma \vdash \mathrm{meet}(p, a, \mathrm{left}(a, b)) \equiv (a, \mathrm{idp}_A(a))}
\end{equation*}

\begin{equation*}
    \frac{\Gamma \vdash A : \UU \hspace{10pt} \Gamma \vdash a : A \hspace{10pt} \Gamma \vdash b : A \hspace{10pt} \Gamma \vdash p : a = b \hspace{10pt} \Gamma \vdash a \not\equiv b}
         {\Gamma \vdash \mathrm{meet}(p, a, \mathrm{right}(a, b)) \equiv (b, p)}
\end{equation*}

\begin{equation*}
    \frac{\Gamma \vdash A : \UU \hspace{10pt} \Gamma \vdash a : A \hspace{10pt} \Gamma \vdash b : A \hspace{10pt} \Gamma \vdash p : a = b \hspace{10pt} \Gamma \vdash H : \partial_A(a, b, a) \hspace{10pt} \Gamma \vdash a \equiv b}
         {\Gamma \vdash \mathrm{meet}(p, a, H) \equiv (a, \mathrm{idp}_A(a))}
\end{equation*}

\begin{equation*}
    \frac{\Gamma \vdash A : \UU \hspace{10pt} \Gamma \vdash a : A \hspace{10pt} \Gamma \vdash b : A \hspace{10pt} \Gamma \vdash p : a = b \hspace{10pt} \Gamma \vdash H : \partial_A(a, b, b) \hspace{10pt} \Gamma \vdash a \equiv b}
         {\Gamma \vdash \mathrm{meet}(p, b, H) \equiv (a, \mathrm{idp}_A(a))}
\end{equation*}

\begin{equation*}
    \frac{\Gamma \vdash A : \UU \hspace{10pt} \Gamma \vdash a : A \hspace{10pt} \Gamma \vdash b : A \hspace{10pt} \Gamma \vdash p : a = b \hspace{10pt} \Gamma \vdash x : A \hspace{10pt} \Gamma \vdash H : \partial_A(a, b, x)}
         {\Gamma \vdash (\mathrm{meet}(p, x, H)).1 \equiv x}
\end{equation*}

\begin{equation*}
    \frac{\begin{matrix}
            \Gamma \vdash A : \UU \hspace{10pt} \Gamma \vdash a : A \hspace{10pt} \Gamma \vdash b : A \\
            \Gamma \vdash B : \prod_{x : A} \partial_A(a, b, x) \rightarrow \UU \hspace{10pt} \Gamma \vdash f : \prod_{x : A} \prod_{H : \partial_A(a, b, x)} B(x, H)
          \end{matrix}}
         {\Gamma \vdash \mathrm{apd}(f, p) : \mathrm{coe}(\mathrm{apd}(B, p), f(a, \mathrm{left}(a, b))) = f(b, \mathrm{right}(a, b))}
\end{equation*}

\begin{equation*}
    \frac{\begin{matrix}
            \Gamma \vdash A : \UU \hspace{10pt} \Gamma \vdash a : A \\
            \Gamma \vdash B : \prod_{x : A} \partial_A(a, a, x) \rightarrow \UU \hspace{10pt} \Gamma \vdash f : \prod_{x : A} \prod_{H : \partial_A(a, a, x)} B(x, H)
          \end{matrix}}
         {\Gamma \vdash \mathrm{apd}(f, \mathrm{idp}_A(a)) \equiv \mathrm{idp}_{B(a, \mathrm{left}(a, a))}(f(a, \mathrm{left}(a, a)))}
\end{equation*}

\begin{equation*}
    \frac{\Gamma \vdash A : \UU \hspace{10pt} \Gamma \vdash a : A \hspace{10pt} \Gamma \vdash b : A \hspace{10pt} \Gamma \vdash B : \UU \hspace{10pt} \Gamma \vdash x : B \hspace{10pt} y, H \notin \mathrm{FV}(B) \cup \mathrm{FV}(x)}
         {\Gamma \vdash \mathrm{apd}(\lambda y.\lambda H.x, p) \equiv \mathrm{idp}_B(x)}
\end{equation*}


\section{Lorem ipsum dolor sit amet}

Lorem ipsum dolor sit amet, consectetur adipiscing elit \cite{DBLP:journals/cacm/Knuth74}. Praesent convallis orci arcu, eu mollis dolor. Aliquam eleifend suscipit lacinia. Maecenas quam mi, porta ut lacinia sed, convallis ac dui. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse potenti. Donec eget odio et magna ullamcorper vehicula ut vitae libero. Maecenas lectus nulla, auctor nec varius ac, ultricies et turpis. Pellentesque id ante erat. In hac habitasse platea dictumst. Curabitur a scelerisque odio. Pellentesque elit risus, posuere quis elementum at, pellentesque ut diam. Quisque aliquam libero id mi imperdiet quis convallis turpis eleifend. 

\begin{lemma}[Lorem ipsum]
\label{lemma:lorem}
Vestibulum sodales dolor et dui cursus iaculis. Nullam ullamcorper purus vel turpis lobortis eu tempus lorem semper. Proin facilisis gravida rutrum. Etiam sed sollicitudin lorem. Proin pellentesque risus at elit hendrerit pharetra. Integer at turpis varius libero rhoncus fermentum vitae vitae metus.
\end{lemma}

\begin{proof}
Cras purus lorem, pulvinar et fermentum sagittis, suscipit quis magna.

\proofsubparagraph*{Just some paragraph within the proof.}
Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
\begin{claim}
content...
\end{claim}
\begin{claimproof}
content...
    \begin{enumerate}
        \item abc abc abc \claimqedhere{}
    \end{enumerate}
\end{claimproof}

\end{proof}

\begin{corollary}[Curabitur pulvinar, \cite{DBLP:books/mk/GrayR93}]
\label{lemma:curabitur}
Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
\end{corollary}

\begin{proposition}\label{prop1}
This is a proposition
\end{proposition}

\autoref{prop1} and \cref{prop1} \ldots

\subsection{Curabitur dictum felis id sapien}

Curabitur dictum \cref{lemma:curabitur} felis id sapien \autoref{lemma:curabitur} mollis ut venenatis tortor feugiat. Curabitur sed velit diam. Integer aliquam, nunc ac egestas lacinia, nibh est vehicula nibh, ac auctor velit tellus non arcu. Vestibulum lacinia ipsum vitae nisi ultrices eget gravida turpis laoreet. Duis rutrum dapibus ornare. Nulla vehicula vulputate iaculis. Proin a consequat neque. Donec ut rutrum urna. Morbi scelerisque turpis sed elit sagittis eu scelerisque quam condimentum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean nec faucibus leo. Cras ut nisl odio, non tincidunt lorem. Integer purus ligula, venenatis et convallis lacinia, scelerisque at erat. Fusce risus libero, convallis at fermentum in, dignissim sed sem. Ut dapibus orci vitae nisl viverra nec adipiscing tortor condimentum \cite{DBLP:journals/cacm/Dijkstra68a}. Donec non suscipit lorem. Nam sit amet enim vitae nisl accumsan pretium. 

\begin{lstlisting}[caption={Useless code.},label=list:8-6,captionpos=t,float,abovecaptionskip=-\medskipamount]
for i:=maxint to 0 do 
begin 
    j:=square(root(i));
end;
\end{lstlisting}

\subsection{Proin ac fermentum augue}

Proin ac fermentum augue. Nullam bibendum enim sollicitudin tellus egestas lacinia euismod orci mollis. Nulla facilisi. Vivamus volutpat venenatis sapien, vitae feugiat arcu fringilla ac. Mauris sapien tortor, sagittis eget auctor at, vulputate pharetra magna. Sed congue, dui nec vulputate convallis, sem nunc adipiscing dui, vel venenatis mauris sem in dui. Praesent a pretium quam. Mauris non mauris sit amet eros rutrum aliquam id ut sapien. Nulla aliquet fringilla sagittis. Pellentesque eu metus posuere nunc tincidunt dignissim in tempor dolor. Nulla cursus aliquet enim. Cras sapien risus, accumsan eu cursus ut, commodo vel velit. Praesent aliquet consectetur ligula, vitae iaculis ligula interdum vel. Integer faucibus faucibus felis. 

\begin{itemize}
\item Ut vitae diam augue. 
\item Integer lacus ante, pellentesque sed sollicitudin et, pulvinar adipiscing sem. 
\item Maecenas facilisis, leo quis tincidunt egestas, magna ipsum condimentum orci, vitae facilisis nibh turpis et elit. 
\end{itemize}

\begin{remark}
content...
\end{remark}

\section{Pellentesque quis tortor}

Nec urna malesuada sollicitudin. Nulla facilisi. Vivamus aliquam tempus ligula eget ornare. Praesent eget magna ut turpis mattis cursus. Aliquam vel condimentum orci. Nunc congue, libero in gravida convallis \cite{DBLP:conf/focs/HopcroftPV75}, orci nibh sodales quam, id egestas felis mi nec nisi. Suspendisse tincidunt, est ac vestibulum posuere, justo odio bibendum urna, rutrum bibendum dolor sem nec tellus. 

\begin{lemma} [Quisque blandit tempus nunc]
Sed interdum nisl pretium non. Mauris sodales consequat risus vel consectetur. Aliquam erat volutpat. Nunc sed sapien ligula. Proin faucibus sapien luctus nisl feugiat convallis faucibus elit cursus. Nunc vestibulum nunc ac massa pretium pharetra. Nulla facilisis turpis id augue venenatis blandit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
\end{lemma}

Fusce eu leo nisi. Cras eget orci neque, eleifend dapibus felis. Duis et leo dui. Nam vulputate, velit et laoreet porttitor, quam arcu facilisis dui, sed malesuada risus massa sit amet neque.

\section{Morbi eros magna}

Morbi eros magna, vestibulum non posuere non, porta eu quam. Maecenas vitae orci risus, eget imperdiet mauris. Donec massa mauris, pellentesque vel lobortis eu, molestie ac turpis. Sed condimentum convallis dolor, a dignissim est ultrices eu. Donec consectetur volutpat eros, et ornare dui ultricies id. Vivamus eu augue eget dolor euismod ultrices et sit amet nisi. Vivamus malesuada leo ac leo ullamcorper tempor. Donec justo mi, tempor vitae aliquet non, faucibus eu lacus. Donec dictum gravida neque, non porta turpis imperdiet eget. Curabitur quis euismod ligula. 


%%
%% Bibliography
%%

%% Please use bibtex, 

\bibliography{lipics-v2021-sample-article}

\appendix

\section{Styles of lists, enumerations, and descriptions}\label{sec:itemStyles}

List of different predefined enumeration styles:

\begin{itemize}
\item \verb|\begin{itemize}...\end{itemize}|
\item \dots
\item \dots
%\item \dots
\end{itemize}

\begin{enumerate}
\item \verb|\begin{enumerate}...\end{enumerate}|
\item \dots
\item \dots
%\item \dots
\end{enumerate}

\begin{alphaenumerate}
\item \verb|\begin{alphaenumerate}...\end{alphaenumerate}|
\item \dots
\item \dots
%\item \dots
\end{alphaenumerate}

\begin{romanenumerate}
\item \verb|\begin{romanenumerate}...\end{romanenumerate}|
\item \dots
\item \dots
%\item \dots
\end{romanenumerate}

\begin{bracketenumerate}
\item \verb|\begin{bracketenumerate}...\end{bracketenumerate}|
\item \dots
\item \dots
%\item \dots
\end{bracketenumerate}

\begin{description}
\item[Description 1] \verb|\begin{description} \item[Description 1]  ...\end{description}|
\item[Description 2] Fusce eu leo nisi. Cras eget orci neque, eleifend dapibus felis. Duis et leo dui. Nam vulputate, velit et laoreet porttitor, quam arcu facilisis dui, sed malesuada risus massa sit amet neque.
\item[Description 3]  \dots
%\item \dots
\end{description}

\cref{testenv-proposition} and \autoref{testenv-proposition} ...

\section{Theorem-like environments}\label{sec:theorem-environments}

List of different predefined enumeration styles:

\begin{theorem}\label{testenv-theorem}
Fusce eu leo nisi. Cras eget orci neque, eleifend dapibus felis. Duis et leo dui. Nam vulputate, velit et laoreet porttitor, quam arcu facilisis dui, sed malesuada risus massa sit amet neque.
\end{theorem}

\begin{lemma}\label{testenv-lemma}
Fusce eu leo nisi. Cras eget orci neque, eleifend dapibus felis. Duis et leo dui. Nam vulputate, velit et laoreet porttitor, quam arcu facilisis dui, sed malesuada risus massa sit amet neque.
\end{lemma}

\begin{corollary}\label{testenv-corollary}
Fusce eu leo nisi. Cras eget orci neque, eleifend dapibus felis. Duis et leo dui. Nam vulputate, velit et laoreet porttitor, quam arcu facilisis dui, sed malesuada risus massa sit amet neque.
\end{corollary}

\begin{proposition}\label{testenv-proposition}
Fusce eu leo nisi. Cras eget orci neque, eleifend dapibus felis. Duis et leo dui. Nam vulputate, velit et laoreet porttitor, quam arcu facilisis dui, sed malesuada risus massa sit amet neque.
\end{proposition}

\begin{conjecture}\label{testenv-conjecture}
Fusce eu leo nisi. Cras eget orci neque, eleifend dapibus felis. Duis et leo dui. Nam vulputate, velit et laoreet porttitor, quam arcu facilisis dui, sed malesuada risus massa sit amet neque.
\end{conjecture}

\begin{observation}\label{testenv-observation}
Fusce eu leo nisi. Cras eget orci neque, eleifend dapibus felis. Duis et leo dui. Nam vulputate, velit et laoreet porttitor, quam arcu facilisis dui, sed malesuada risus massa sit amet neque.
\end{observation}

\begin{exercise}\label{testenv-exercise}
Fusce eu leo nisi. Cras eget orci neque, eleifend dapibus felis. Duis et leo dui. Nam vulputate, velit et laoreet porttitor, quam arcu facilisis dui, sed malesuada risus massa sit amet neque.
\end{exercise}

\begin{definition}\label{testenv-definition}
Fusce eu leo nisi. Cras eget orci neque, eleifend dapibus felis. Duis et leo dui. Nam vulputate, velit et laoreet porttitor, quam arcu facilisis dui, sed malesuada risus massa sit amet neque.
\end{definition}

\begin{example}\label{testenv-example}
Fusce eu leo nisi. Cras eget orci neque, eleifend dapibus felis. Duis et leo dui. Nam vulputate, velit et laoreet porttitor, quam arcu facilisis dui, sed malesuada risus massa sit amet neque.
\end{example}

\begin{note}\label{testenv-note}
Fusce eu leo nisi. Cras eget orci neque, eleifend dapibus felis. Duis et leo dui. Nam vulputate, velit et laoreet porttitor, quam arcu facilisis dui, sed malesuada risus massa sit amet neque.
\end{note}

\begin{note*}
Fusce eu leo nisi. Cras eget orci neque, eleifend dapibus felis. Duis et leo dui. Nam vulputate, velit et laoreet porttitor, quam arcu facilisis dui, sed malesuada risus massa sit amet neque.
\end{note*}

\begin{remark}\label{testenv-remark}
Fusce eu leo nisi. Cras eget orci neque, eleifend dapibus felis. Duis et leo dui. Nam vulputate, velit et laoreet porttitor, quam arcu facilisis dui, sed malesuada risus massa sit amet neque.
\end{remark}

\begin{remark*}
Fusce eu leo nisi. Cras eget orci neque, eleifend dapibus felis. Duis et leo dui. Nam vulputate, velit et laoreet porttitor, quam arcu facilisis dui, sed malesuada risus massa sit amet neque.
\end{remark*}

\begin{claim}\label{testenv-claim}
Fusce eu leo nisi. Cras eget orci neque, eleifend dapibus felis. Duis et leo dui. Nam vulputate, velit et laoreet porttitor, quam arcu facilisis dui, sed malesuada risus massa sit amet neque.
\end{claim}

\begin{claim*}\label{testenv-claim2}
Fusce eu leo nisi. Cras eget orci neque, eleifend dapibus felis. Duis et leo dui. Nam vulputate, velit et laoreet porttitor, quam arcu facilisis dui, sed malesuada risus massa sit amet neque.
\end{claim*}

\begin{proof}
Fusce eu leo nisi. Cras eget orci neque, eleifend dapibus felis. Duis et leo dui. Nam vulputate, velit et laoreet porttitor, quam arcu facilisis dui, sed malesuada risus massa sit amet neque.
\end{proof}

\begin{claimproof}
Fusce eu leo nisi. Cras eget orci neque, eleifend dapibus felis. Duis et leo dui. Nam vulputate, velit et laoreet porttitor, quam arcu facilisis dui, sed malesuada risus massa sit amet neque.
\end{claimproof}

\end{document}
